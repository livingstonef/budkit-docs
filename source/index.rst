===================================
Welcome to the Budkit documentation
===================================

BudKit (BK) is a free open-source platform for building applications for social and professional networking.

.. toctree::
   :maxdepth: 2

   introduction
   requirements
   installation
   changelog
   license
   glossary
   manual/index
   developers/index


