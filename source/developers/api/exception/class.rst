==================
Library\\Exception
==================

.. php:namespace:: Library

.. php:class:: Exception

      A custom exception handler

   .. php:method:: Exception::log()

      Logs an exception

      :returns: void

   .. php:method:: Exception::getInstance()

      Gets an instance of the Library\\Exception Object

      :staticvar: Exception $instance

      :returns: Exception