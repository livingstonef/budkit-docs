================
Library\\Encrypt
================

.. php:namespace:: Library

.. php:class:: Encrypt

   .. php:attr:: $key

      The globally defined encryption Key

      :var: string

   .. php:method:: Encrypt::__construct()

      Encryption clas constructor
      Loads the encryption configuration

      :returns: void

   .. php:method:: Encrypt::generateKey()

      Generates a random encryption key

      :param string $txt:

      :returns: string

   .. php:method:: Encrypt::encode()

      Encodes a given string

      :param string $string:

      :returns: string $ncoded string

   .. php:method:: Encrypt::getKey()

      Returns the protected encryption key

      NOTE: This method is left public, because the session might need to know
      what the encryption key is, to decipher session keys. @TODO fix this and
      make this method protected or private

      :property-read: string $key The encryption key property

      :returns: string

   .. php:method:: Encrypt::decode()

      Decodes a previously encode string.

      :param string $encrypted:

      :returns: string $ecoded string

   .. php:method:: Encrypt::mcryptEncode()

   .. php:method:: Encrypt::mcryptDecode()

      Decrypts a previously encrypted text with given parameters

      :param type $cipher:
      :param type $key:
      :param type $data:
      :param type $mode:

      :returns: string

   .. php:method:: Encrypt::mcryptExists()

      Checks if we can use mcrypt for encryption

      :returns: boolean $rue or False if encrypt exists

   .. php:method:: Encrypt::hash()

      Returns an sha1, MD5 and sha224 hash (in that order) of a given string

      :param string $string:
      :param string $key:

      :returns: string

   .. php:method:: Encrypt::getInstance()

      Returns an instance of the encrypt class

      :staticvar: self $instance

      :returns: self