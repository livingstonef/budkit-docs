===============
Library\\Folder
===============

.. php:namespace:: Library

.. php:class:: Folder

      Folder handling methods

   .. php:attr:: $instance

   .. php:method:: Folder::getModifiedDate()

      Returns the UNIX timestamp representation
      of the last time the folder was modified

      :param string $path:

   .. php:method:: Folder::getSize()

      Returns in int representation of the file size in bytes

      :param string $path:

   .. php:method:: Folder::name()

      Gets the name of the folder

      :param string $path:

   .. php:method:: Folder::create()

      Create a folder if not exits

      :param string $path:

      :returns: boolean $rue if success, false on failure, or nothing if folder exists

   .. php:method:: Folder::move()

      Moves the folder to a new location

      :param type $path:
      :param type $toPath:
      :param type $replace:

      :todo: Will $lways replace for now.

   .. php:method:: Folder::copy()

      Copies the file or folder to a new destination

      :param type $path:
      :param type $toPath:
      :param type $replace:

      :todo: Will $lways replace for now.

   .. php:method:: Folder::remove()

      Deletes a folder

      :param type $path:
      :param type $backup:

   .. php:method:: Folder::hasBackup()

      Check if a folder has a backup

      :param type $path:

   .. php:method:: Folder::restoreBackup()

      Restore backup

      :param type $path:

   .. php:method:: Folder::chmod()

      Sets file permission i.e change the file mode

      :param type $path:
      :param type $permission:

      :returns: \\Library\\Folder\\Files

   .. php:method:: Folder::getPermission()

      Gets file permissions

      :param type $path:

   .. php:method:: Folder::chmodR()

      Recursively sets permissions for all files in a folder

      :param type $path:
      :param type $permission:

   .. php:method:: Folder::pack()

      :param type $path:
      :param type $type:

   .. php:method:: Folder::itemize()

      Lists all the files in a directory

      ath/to/folder" => array(
      ame" => '',
      arent" => '', //only in long
      ize" => '', //only in long
      odified" => '', //only in long
      ermission" => '',
      iles" => array(
      ath/to/file" => array(
      ame" => '',
      ize" => '', //only in long
      odified" => '', //only in long
      ermission" => '',
      xtension"  => '',
      imetype"   => ''//only in long

      hildren" => array(
      Contains a list of all sub folders,
      *recursion*

      :param string $path: the compound path being searched and listed
      :param array $exclude: a list of folders, files or fileTypes to exclude from the list
      :param boolean $recursive: Determines whether to search subdirectories if found
      :param interger $recursivelimit: The number of deep subfolder levels to search
      :param boolean $showfiles: Include Files contained in each folder to the array
      :param boolean $sort: Sort folder/files in alphabetical order
      :param boolean $long: returns size, permission, datemodified in list if true, Slow!!

      :returns: array $list = array(

   .. php:method:: Folder::itemizeFind()

      Finds and return the folder list matching $name in $inPath.
      Use $limit to define how many occurences to return if found, default is 1
      Method will therefore stop once the number of found response is = $limit, use $limit = 0 to find all

      :param string $name:
      :param string $inPath:
      :param interger $limit:
      :param boolean $recursive:
      :param interger $recursiveLimit:
      :param boolean $showfiles:
      :param boolean $sort:
      :param boolean $long:

   .. php:method:: Folder::is()

      Determines if a path links to a folder or file

      :param string $path:
      :param boolean $folder,: value to return if is folder

   .. php:method:: Folder::deleteContents()

      Method to delete the contents of a folder

      :param type $folderpath:
      :param type $filterByType:
      :param type $filterByName:
      :param type $filterExcludeMode:

   .. php:method:: Folder::delete()

      Deletes a file or folder if exists

      :param type $path:

   .. php:method:: Folder::exists()

      Determines if a path is credible

      :param type $path:

   .. php:method:: Folder::isWritable()

      Checks if a file or folder is writable

      :param type $path:

   .. php:method:: Folder::getInstance()

      Returns an instance of the folder object

      :staticvar: self $instance

      :returns: self

   .. php:method:: Folder::getFile()

      Returns an instance of the file object

      :returns: File $lass Pdf | Image | Xml

   .. php:method:: Folder::getPacker()

      Returns an instance of the archiver object

      :param type $type:

      :returns: type