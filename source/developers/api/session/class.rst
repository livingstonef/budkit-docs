================
Library\\Session
================

.. php:namespace:: Library

.. php:class:: Session

   .. php:attr:: $id

      :var: type

   .. php:attr:: $registry

      :var: array

   .. php:method:: Session::__construct()

      Constructs the session Object

      :param type $namespace:

   .. php:method:: Session::close()

      Writes session data and ends the session

      :returns: void

   .. php:method:: Session::getUploadProgressName()

      Gets the session progess upload name;

      :returns: type

   .. php:method:: Session::getUploadProgress()

      Gets the Upload progressName

      :param type $formName:

      :returns: int

   .. php:method:: Session::start()

      Starts a session

      :param type $killPrevious:

      :returns: void

   .. php:method:: Session::getSplash()

      Gets session parameters and develop a 'splash'

      :returns: type

   .. php:method:: Session::create()

      Creates a new session.

      Will only create a session if none is found
      The 'default' and 'auth' namespaces are also added
      The 'auth' namespace is locked and unwrittable,

      :returns: void

   .. php:method:: Session::getAuthority()

      Reads the authenticated user's right,

      :returns: Authority

   .. php:method:: Session::isAuthenticated()

      Determines whether a user has been
      Authenticated to this browser;

      :returns: boolean $rue or false depending on auth status

   .. php:method:: Session::generateToken()

      Generates a random token

      :returns: type

   .. php:method:: Session::getNamespace()

      Gets a namespaced session registry

      :param type $name:

      :returns: type

   .. php:method:: Session::generateId()

      Generates the session Id

      :param type $splash:

      :returns: type

   .. php:method:: Session::read()

      Reads session data from session stores

      :param string $id:

      :returns: Boolean $alse on failed and session ID on success

   .. php:method:: Session::update()

      Updates a user session store on state change

      :param type $sessId:
      :param type $userdata:

      :returns: type

   .. php:method:: Session::restart()

      Destroys any previous session and starts a new one
      Hopefully

      :returns: void

   .. php:method:: Session::destroy()

      Destroys a redundant session

      :param type $id:
      :param type $restart:

   .. php:method:: Session::write()

      Writes data to session stores

      :param type $data:
      :param type $sessId:

      :returns: type

   .. php:method:: Session::setOptions()

      Sets session options

      :param type $options:

   .. php:method:: Session::maxTimeToExpire()

      Sets the maximum time for sessin to expire
      Expires the session id in x time
      If x is = zero, set expire time to 50 days from now (60*60*24*50*1);

      :returns: void

   .. php:method:: Session::maxRequestToExpire()

      Sets the session maximum number of rrequests allowed
      Expires the session id in x Requests
      if x is = zero, allow unlimited requests

      :returns: void;

   .. php:method:: Session::getRequestCount()

      Returns the total requests made in this session

      :returns: interger

   .. php:method:: Session::lock()

      Locks a namespaced registry to prevent further edits

      :param type $namespace:

      :returns: type

   .. php:method:: Session::isLocked()

      Determines whether a namespaced registry is locked

      :param type $namespace:

      :returns: type

   .. php:method:: Session::unlock()

      Unlocks a registry.

      BEWARE: Some registry items are locked for better performance. Do not
      Unlock unless you absolutely need to, better still if you really need
      flexibility, create your own namespaced registry

      :param type $namespace:

      :returns: type

   .. php:method:: Session::getId()

      Retuns the session Id

      :returns: type

   .. php:method:: Session::get()

      Gets a namespaced registry value, stored in the session

      :param type $varname:
      :param type $namespace:

      :returns: type

   .. php:method:: Session::set()

      Sets a value for storage in a namespaced registry

      :param type $varname:
      :param type $value:
      :param type $namespace:

      :returns: Session

   .. php:method:: Session::remove()

      Removes a value from the session registry

      :param type $varname:
      :param type $value:
      :param type $namespace:

   .. php:method:: Session::getInstance()

      Gets an instance of the session Object

      :staticvar: self $instance

      :returns: self

   .. php:method:: Session::__destruct()

      A destructor method for the session class

      :returns: void