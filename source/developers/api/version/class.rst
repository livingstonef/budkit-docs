=================
Platform\\Version
=================

.. php:namespace:: Platform

.. php:class:: Version

      Handles versioning

      The version class is preferabbly used as static. Methods include those for
      returning the system version number in either long or short formats.
      The long format will return information about the current build number, release
      date, status, codename, and license information.
      The Short format returns just the system version number in conventational formats
      And should be used when comparing system version.

   .. php:attr:: $PRODUCT

   .. php:attr:: $RELEASE

   .. php:attr:: $DEV_STATUS

   .. php:attr:: $DEV_LEVEL

   .. php:attr:: $BUILD

   .. php:attr:: $CODENAME

   .. php:attr:: $RELDATE

   .. php:attr:: $RELTIME

   .. php:attr:: $REVISION

   .. php:attr:: $RELTZ

   .. php:attr:: $COPYRIGHT

   .. php:attr:: $URL

   .. php:attr:: $DEV_BUG

   .. php:method:: Version::getLongVersion()

      Long veresion

      :returns: string $ong format version

   .. php:method:: Version::getShortVersion()

      Short Version

      :returns: string $hort version format

   .. php:method:: Version::getShortVersionBuild()

      :returns: string $hort version format

   .. php:method:: Version::isCompatible()

      Compares two "A PHP standardized" version number against the current Joomla! version

      :returns: boolean

      :see: http://www.php.net/version_compare

   .. php:method:: Version::isOutDated()

      Checks if the current version of Tuiyo is out of date

      :param mixed $latest:

      :returns:

   .. php:method:: Version::getInstance()

      Gets an instance of the version class

      :staticvar: object $instance

      :returns: object $ersion