====================
Platform\\Controller
====================

.. php:namespace:: Platform

.. php:class:: Controller

   .. php:attr:: $instance

   .. php:attr:: $controller

      The controller defined in the route mapp

      :var: string

   .. php:attr:: $method

      The method (task/action) to be executed by the dispatcher

      :var: string

   .. php:attr:: $displayed

      Determines whether the output has been displayed or not

      :var: boolean

   .. php:attr:: $redirect

      Sets for redirect

      :var: string

   .. php:method:: Controller::__construct()

      Constructor for the application controller
      Must be called from child controller, getInstance

      :void:

   .. php:method:: Controller::getMethod()

      Returns the name of  method/task

      :returns: string

   .. php:method:: Controller::returnRequest()

      Returns the request back to referer;

      :returns: boolean $changes state?)

   .. php:method:: Controller::getController()

      Gets the name of the action controller

      :returns: string

   .. php:method:: Controller::requireAuthentication()

      Checks authenticated status of $this->user;

      :returns: type

   .. php:method:: Controller::getApplication()

   .. php:method:: Controller::alert()

      Displays messages on output

      :param string $message:
      :param string $title:
      :param string $type: | information, error, success, attention, note etc.

   .. php:method:: Controller::getRequestArgs()

      Returns the arguments defined in the URL

      :returns: array

   .. php:method:: Controller::set()

      Sets a output property for later use.

      :param string $name:
      :param mixed $value:

   .. php:method:: Controller::getAuthority()

      Determines the current user authority

   .. php:method:: Controller::setredirect()

      Redirects only after the action is completely executed

      :param type $url:

   .. php:method:: Controller::redirect()

      Stops executing the action and redirects

      :param type $url:

   .. php:method:: Controller::login()

      The login action

   .. php:method:: Controller::logout()

   .. php:method:: Controller::render()

   .. php:method:: Controller::index()

      This is the default method for undispatchable action=>tasks
      Iterim search for the possibility that the path is virtual

      :returns: redirect $o start page

   .. php:method:: Controller::__destruct()

      Displays the output for the request;

      :returns: