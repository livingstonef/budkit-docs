======================
Platform\\Authenticate
======================

.. php:namespace:: Platform

.. php:class:: Authenticate

   .. php:attr:: $userid

      :var: type

   .. php:attr:: $type

   .. php:attr:: $username

   .. php:attr:: $password

   .. php:attr:: $email

   .. php:attr:: $fullname

   .. php:attr:: $language

   .. php:attr:: $timezone

   .. php:attr:: $authenticated

   .. php:method:: Authenticate::__construct()

      Constructor

      :param string $name: The type of the response

      :since: 1.0.0

      :returns: void

   .. php:method:: Authenticate::getInstance()

      Returns an instance of the authentication class

      :staticvar: self $instance
      :param array $splash:

      :property-write: string $email

      :property-write: string $fullname

      :property-write: string $language

      :property-write: string $password

      :property-write: string $timezone

      :property-write: string $userid

      :property-write: string $type

      :property-read: object $instance

      :property-write: object $instance

      :returns: self