===============
Platform\\Model
===============

.. php:namespace:: Platform

.. php:class:: Model

   .. php:attr:: $dao

      Data access objects

      :var: type

   .. php:attr:: $states

      The current state or possition in the system

      :var: static $state

   .. php:attr:: $total

      The current total in a navigable record set

      :var: static $otal

   .. php:attr:: $pagination

      The current state of the pages menu

      :var: static $agination

   .. php:method:: Model::__construct()

      Class constructor,
      Instantiates helper classes

      return void

   .. php:method:: Model::setTable()

      Sets the model table

      :param type $table:

   .. php:method:: Model::getTable()

      Gets table representation

   .. php:method:: Model::set()

      Sets an output

      :param type $name:
      :param type $value:

   .. php:method:: Model::get()

   .. php:method:: Model::display()

      The default method for each controller

      :returns: void

   .. php:method:: Model::getInstance()

      Instantiate the child controller

      :returns: object

   .. php:method:: Model::getState()

      Returns a data model state

      :param type $state:

   .. php:method:: Model::setState()

      Sets a data model state

      :param type $state:
      :param type $value:

   .. php:method:: Model::setListLimit()

      Sets lists limit for page'd lists

      :param type $limit:

      :returns: \\Platform\\Entity

   .. php:method:: Model::getListOffset()

      Get list start for page'd lists

      :param type $start:

      :returns: \\Platform\\Entity

   .. php:method:: Model::getListLimit()

      Gets lists limit for page'd lists

      :param type $limit:

      :returns: \\Platform\\Entity

   .. php:method:: Model::setListOffset()

      Set list start for page'd lists

      :param type $start:

      :returns: \\Platform\\Model

   .. php:method:: Model::setListTotal()

      Sets the list total

      :param type $total:

      :returns: \\Platform\\Model

   .. php:method:: Model::getListTotal()

      Returns the list total;

      :returns: type

   .. php:method:: Model::getLimitClause()

      Returns a limit clause based on datamodel limit and limitoffset states

      :returns: type

   .. php:method:: Model::setPagination()

      Sets the pagination for the current output if any

      :returns: type

   .. php:method:: Model::__destruct()

      Displays the output for the request;

      :returns: