==================
Platform\\Debugger
==================

.. php:namespace:: Platform

.. php:class:: Debugger

   .. php:attr:: $time

   .. php:attr:: $memory

   .. php:method:: Debugger::getCallStackDump()

   .. php:method:: Debugger::getInstance()

   .. php:method:: Debugger::start()

      Records the debugger and system start time

      :returns: void

   .. php:method:: Debugger::log()

      Provides an alias for the log message method

      :param mixed $string:
      :param string $title:
      :param string $type:
      :param string $typekey:
      :param boolean $console:
      :param boolean $logFile:

      :returns: static::_()

   .. php:method:: Debugger::stop()

      Records the debugger stop and stystem stop time
      Ideally the last method to be called before the output is sent to the server

      :returns: void

   .. php:method:: Debugger::__construct()

   .. php:method:: Debugger::__desstruct()