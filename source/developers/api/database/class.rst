=================
Library\\Database
=================

.. php:namespace:: Library

.. php:class:: Database

      Database abstraction handler

      This Database class is a database independent query interface definition.
      It allows you to connect to different data sources like MySQL, SQLite and
      other RDBMS on a Win32 operating system. Moreover the possibility exists to
      use MS Excel spreadsheets, XML, text files and other not relational data
      as data source.

   .. php:attr:: $resourceId

      The database connection resource id

      :var: resource

   .. php:attr:: $driver

      The current driver being used

      :var: string

   .. php:attr:: $query

      The last query to be executed in this connection

      :var: string

   .. php:attr:: $offset

      Offset Value

      :var: interger

   .. php:attr:: $limit

      A limit for the resultset

      :var: interger

   .. php:attr:: $ticker

   .. php:attr:: $tMode

      Method to check that we are in transaction mode

      :var: boolean

   .. php:attr:: $debug

      Database debug mode

      :var: boolean

   .. php:method:: Database::getResourceId()

      Returns the datbase connection resource ID

      :returns: bool $ALSE if not connected / ID if found

   .. php:method:: Database::prepare()

      Prepares an SQL query for execution;

      :param string $statement:

      :returns: object $Library\Database\Results

   .. php:method:: Database::getDriver()

      Returns the current driver object

      :returns: Object

   .. php:method:: Database::getTotalQueryCount()

      Returns the total number of Queries executed thus far

      :returns: interger

   .. php:method:: Database::getQueryLog()

      Returns a log of total number of Queries executed thus far

      :returns: array

   .. php:method:: Database::__call()

      For active record querying ONLY

      :param string $method:
      :param mixed $args:

      :returns: mixed

   .. php:method:: Database::getInstance()

      Returns an instance of the database object for the active driver

      :staticvar: self $instance
      :param array $options:

      :returns: object $atabase

   .. php:method:: Database::quote()

      Quotes a string in a query,

      :param string $text:
      :param boolean $escaped:

      :returns: string $uoted string

   .. php:method:: Database::replacePrefix()

      This function replaces a string identifier <var>$prefix</var> with the
      string held is the <var>_table_prefix</var> class variable.

      :access: public
      :param string $he: SQL query
      :param string $he: common table prefix

      :returns: void

   .. php:method:: Database::__destruct()

      Object destructor, must be declared in all drivers

      :returns: void

   .. php:method:: Database::close()

      Closes the database connection

      :returns: void

   .. php:method:: Database::isConnected()

      Determine if the database object is connected to a DBMS

      :returns: void

   .. php:method:: Database::getEscaped()

      Custom driver text escaping, Must be defined in the driver

      :returns: void

   .. php:method:: Database::getVersion()

      Gets the version of the currrent DBMS driver

      :returns: void

   .. php:method:: Database::connect()

      Driver connect method

      :returns: boolean

   .. php:method:: Database::test()

      Custom Tests, For connectivity test, use Database::isConnected

      :returns: void

   .. php:method:: Database::connected()

      Alias of Database isConected method

      :returns: void

   .. php:method:: Database::hasUTF()

      Determines if the database driver, has UTF handling capabilities
      And returns its default settings

      :returns: void

   .. php:method:: Database::setUTF()

      Sets the UTF Charset type

      :returns: void

   .. php:method:: Database::exec()

      Executes a predifined query

      :returns: void

   .. php:method:: Database::startTransaction()

      Begins a database transaction

      :returns: void;

   .. php:method:: Database::query()

      This method is intended for use in transsactions

      :returns: boolean

   .. php:method:: Database::commitTransaction()

      Commits a transaction or rollbacks on error

      :returns: boolean