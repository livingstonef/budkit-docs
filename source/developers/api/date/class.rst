=============
Library\\Date
=============

.. php:namespace:: Library

.. php:class:: Date

   .. php:attr:: $timestamp

   .. php:method:: Date::setDefaultTimeZone()

   .. php:method:: Date::today()

      Returns todays date timestamp

      :returns: string

   .. php:method:: Date::yesterday()

      Returns yesterdays date timestamp

      :returns: string

   .. php:method:: Date::translate()

      Translated from string to date

      :param string $timestring:

      :returns: string $ well formated date

   .. php:method:: Date::getTime()

      Returns the timestamp for the current date

      :returns: string

   .. php:method:: Date::getInstance()

      Get's an instance of the Date time object

      :staticvar: self $instance

      :returns: self