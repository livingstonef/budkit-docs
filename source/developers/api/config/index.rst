=============
Configuration
=============

Budkit configuration directives are loaded into a 'globally' available ``$config`` array from various locations, including
the database stores. Developers should refer to the :doc:`the config class <class>` for information on how
to manage their app parameters. This section describes the default config params, and their default values

-------
Session
-------

**store** : *file*
    Change to *database* to use database as session handler

**table** : `?session`
    If the `store` param above is set to database, change here for the database table to use. the "?" prefix will be
    replaced with the installed database table prefix

**cookie** : *psession*
    The name of the session cookie.

**domain** : *budkit*
    The session cookie domain.

**path** : `/`

datafile : `/`
    The absolute path to the session data file store

life : `60 * 15 * 4`
    The ife of a stored session.

folder : `/sessions`

remember : `60 * 60 * 24 * 14`
    The length of a session. If no explicit expiry time is set, session will be remembered for this long