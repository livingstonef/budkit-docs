===============
Library\\Config
===============

.. php:namespace:: Library

.. php:class:: Config

      What is the purpose of this class, in one sentence?

      How does this class achieve the desired purpose?

      :category: Library

      :author: $Livingstone Fultang

      :copyright: 1997-2012 $tonyhills HQ

      :license: http://www.gnu.org/licenses/gpl.txt. $NU GPL License 3.01

      :version: Release: $.0.0

      :since: Class $vailable since Release 1.0.0 Jan 14, 2012 4:54:37 PM

   .. php:attr:: $database

      The Config Database Adaptor

      :var: type

   .. php:attr:: $xml

      The Config XML Adaptor

      :var: type

   .. php:attr:: $ini

      The Config INI Adaptor

      :var: type

   .. php:method:: Config::__construct()

      Constructor for the cofig library

      :returns: void;

   .. php:method:: Config::getParams()

      Returns the configuration array

      :returns: array

   .. php:method:: Config::setParam()

      Sets a param in a configuration section section.

      Use setParamSection to create a section
      if it does not exists before creating one. Use hasParamSection to check for
      the existence of that specific section before creating one.

      :param string $name: The name of the param to update
      :param mixed $value: The value of the param
      :param string $section: Default section is the system section

      :returns: Return $alse if the section does not exists.

   .. php:method:: Config::getParam()

      Returns a config Item, from configuration

      :param type $name:
      :param type $default:
      :param type $section:
      :param type $adapter:

   .. php:method:: Config::setParamSection()

      Sets Param section to the config array, or replaces one if already exists

      NOTE: You will need to explicitly saveParams with a handler to add this conifugration permanently

      Example:
      The following usage will add an save configuration data on the fly to the database;
      $config->setParamSection("userdata", array("param1"=>"param1value") );
      $config->database->saveParams("userdata");

      :param type $name:
      :param type $params:

   .. php:method:: Config::getParamSection()

      Returns a config group/section element

      :param type $section:

      :returns: type

   .. php:method:: Config::getUserPreferences()

      Preferences are user configs stored in site-users-folders

      :param type $user_name_id:

      :returns: array

   .. php:method:: Config::getConfig()

      Loads all the system configuration

      :staticvar: type $configarray
      :param type $ext:
      :param type $path:

      :returns: type

   .. php:method:: Config::getDBParams()

      :param type $section:
      :param type $autoload:

   .. php:method:: Config::mergeParams()

      Recursively Merges two params arrays, overwriting values in params1 with those in params2

      :param type $params1:
      :param type $params2:

   .. php:method:: Config::getDefinition()

      Parses a config definition file

      :param string $xml:
      :param string $type:

      :returns: boolean $alse if no config found or config object if found

   .. php:method:: Config::getInstance()

      Gets an instance of the config element

      :property-read: object $instance

      :property-write: object $instance

      :staticvar: self $instance

      :returns: self