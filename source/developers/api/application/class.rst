=====================
Platform\\Application
=====================

.. php:namespace:: Platform

.. php:class:: Application

   .. php:attr:: $object

      Stores an application object

      :var: array

   .. php:method:: Application::getInstance()

      Gets an instance of the Application Platform Object

      :staticvar: self $instance

      :returns: self