==================
Platform\\Registry
==================

.. php:namespace:: Platform

.. php:class:: Registry

   .. php:attr:: $variables

   .. php:method:: Registry::setVar()

   .. php:method:: Registry::getVar()

   .. php:method:: Registry::issetVar()

   .. php:method:: Registry::unsetVar()

   .. php:method:: Registry::getInstance()

      Gets an instance of the Framework Class

      :staticvar: self $instance

      :returns: self