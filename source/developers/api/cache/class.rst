==============
Library\\Cache
==============

.. php:namespace:: Library

.. php:class:: Cache

   .. php:attr:: $instance

   .. php:method:: Cache::__construct()

   .. php:method:: Cache::hasDriver()

   .. php:method:: Cache::read()

   .. php:method:: Cache::delete()

   .. php:method:: Cache::save()

   .. php:method:: Cache::clean()

   .. php:method:: Cache::readMeta()

   .. php:method:: Cache::info()

   .. php:method:: Cache::getInstance()

      Returns an instance of the cache Class

      :staticvar: \\Library\\Cache $instance

      :returns: \\Library\\Cache