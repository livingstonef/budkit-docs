===============
Library\\Output
===============

.. php:namespace:: Library

.. php:class:: Output

   .. php:attr:: $positions

      A list of positions in the main theme

      :var: array

   .. php:attr:: $isError

      :var: type

   .. php:attr:: $layout

      Defines the layout to be used

      :var: string

   .. php:attr:: $layoutExt

      Defines the layout to be used

      :var: string

   .. php:attr:: $message

      A message to be displayed on the page

      :var: string

   .. php:attr:: $variables

      Additional page variables

      :var: array

   .. php:attr:: $instance

   .. php:attr:: $scripts

      Contains a list of references to scripts
      to include on this page

      :var: array

   .. php:attr:: $meta

      Contains a list of page meta

      :var: array

   .. php:attr:: $styles

      An array of Stylesheet references to be included on page

      :var: array

   .. php:attr:: $config

      Reference to the configuration object

      :var: object $ibrary\Config

   .. php:attr:: $code

      The response code.

      :var: interger;

   .. php:attr:: $format

   .. php:attr:: $prints

   .. php:attr:: $headers

   .. php:attr:: $menugroups

   .. php:method:: Output::__construct()

      Construcst the output object

      :returns: void

   .. php:method:: Output::getVariables()

      Returns all the protected output variables

      :returns: array;

   .. php:method:: Output::getHeaders()

      Returns all the protected output variables

      :returns: array;

   .. php:method:: Output::recallAlerts()

      Recalls all messages sent to client before redirect
      These are stored in the session

      :returns: void

   .. php:method:: Output::setFormat()

      Sets the output format, overwrites format set in router?

      :param type $format:

      :returns: void

   .. php:method:: Output::startBuffer()

      Starts the output buffer

      :handler: the $b handler callback; NOTE, use null rather than "" for no handler

      :returns: void

   .. php:method:: Output::stopBuffer()

   .. php:method:: Output::link()

      Outputs an internalized resource link

      :param type $href:

      :returns: type

   .. php:method:: Output::displayError()

      Displays the error page

      :param type $format:
      :param type $status:

      :returns: void

   .. php:method:: Output::navigation()

      Outputs a menu  item to the output buffer

      :param type $menuid:
      :param type $menutype:

   .. php:method:: Output::restartBuffer()

      Restarts the output buffer

      :handler: the $b handler callback; NOTE, use null rather than "" for no handler

      :returns: type

   .. php:method:: Output::getHandler()

   .. php:method:: Output::display()

      Returns the processed output and displays to the browser
      Final method cannot and should not be overidden

      :param string $format:

      :returns: void

   .. php:method:: Output::serialize()

      Serializes data input

      :param type $data:

      :returns: type

   .. php:method:: Output::unserialize()

      Unserializes are previously serialized input

      :param type $string:

      :returns: type

   .. php:method:: Output::getRandomString()

      :param type $length:

      :returns: string

   .. php:method:: Output::setCookie()

      Sets a cookie param

      :param type $name:
      :param type $cookie:
      :param type $expire:
      :param type $path:
      :param type $domain:

      :returns: Output

   .. php:method:: Output::layout()

      Parses a layout and sets the variable ass

      :param type $layout:
      :param type $variables:
      :param type $set:
      :param type $setas:

   .. php:method:: Output::setLayout()

      Sets a reference to the layout object being used

      :param string $layout:

   .. php:method:: Output::getTemplateName()

      Alias method to return the string name of the current template

      :returns: string

   .. php:method:: Output::getTemplatePath()

      Returns the current template path;

      :returns: string;

   .. php:method:: Output::setHeader()

      Adds a header type to the output

      :param type $name:
      :param type $value:

   .. php:method:: Output::unsetHeader()

      Removes a previously sent header type to the output

      :param type $name:
      :param type $value:

   .. php:method:: Output::headers()

      Set Header

      Sets the page header, depending on the requested format

      :returns: void

   .. php:method:: Output::addScript()

      Adds a reference to a script resource to include on the
      page

      :param string $file:

   .. php:method:: Output::getScripts()

      Returns an array of dynamically added page scripts

      :returns: type

   .. php:method:: Output::getStyles()

      Returns an array of dynamically added page styles

      :returns: type

   .. php:method:: Output::getMeta()

      Returns an arry of dymaically added page meta

      :returns: type

   .. php:method:: Output::addStyle()

      Adds a reference to a stylesheet resource to include on the page

      :param string $href:
      :param string $rel:
      :param string $type:
      :param string $media:
      :param string $sizes:
      :param string $hreflang:

      :returns: \\Library\\Output

   .. php:method:: Output::addMeta()

      Add Meta

      :param string $name:
      :param string $content:
      :param string $charset:
      :param string $httpequiv:

      :returns: \\Library\\Output

   .. php:method:: Output::addMessage()

      Add Message

      Displays a message of specific type on the page

      :param string $message:
      :param string $type:

   .. php:method:: Output::addVariable()

      Add Variable

      Adds a page variable

      :param string $name:
      :param string $value:
      :param boolean $returnprevious:

      :returns: Output

   .. php:method:: Output::addLinkToMenuGroup()

      Adds a custom link to a menugroup.

      :param type $menuNameId:
      :param type $link:

   .. php:method:: Output::getMenuGroups()

      Returns all the defined menugroups

      :returns: type

   .. php:method:: Output::getMenuGroup()

      Returns the menu group

      :param type $groupName:
      :param type $default:

      :returns: type

   .. php:method:: Output::addMenuGroupToPosition()

      Add a menu to a block position

      :param type $blockName:
      :param type $menuNameId:
      :param type $menuType:
      :param type $menuItems:
      :param type $overwrite:
      :param type $icons:

      :returns: \\Library\\Output

   .. php:method:: Output::getTemplate()

      Returns the template folder name

      :returns: type

   .. php:method:: Output::setMimeType()

      Sets the page MimeType

      :returns: object $utput

   .. php:method:: Output::setResponseCode()

      Sets the application response code

      :param type $code:

      :returns: Output

   .. php:method:: Output::getResponseCode()

      Sets the application response code

      :param type $code:

      :returns: Output

   .. php:method:: Output::setPageTitle()

      Adds a page title

      :returns: string

   .. php:method:: Output::set()

      Sets a final page element, see @method addVariable

      :param string $param:
      :param mixed $value:

      :returns: object $utput

   .. php:method:: Output::removeOutputVar()

      Removes an output variable

      :param type $param:

      :returns: boolean

   .. php:method:: Output::get()

      Gets a page element

      :param string $param:
      :param mixed $default:
      :param mixed $format:

   .. php:method:: Output::__get()

      Output Magic variable Getter

      :param type $name:

      :returns: type

   .. php:method:: Output::getMimeType()

      Returns the current page mimiType if any

      :returns: string

   .. php:method:: Output::getPageTitle()

      Returns the page title

      :returns: string

   .. php:method:: Output::getLangauge()

      Gets the output language

      :returns: string

   .. php:method:: Output::getPageDescription()

      Returns a description for the current page

      :returns: string

   .. php:method:: Output::getPageAuthor()

      Returns an Author for the current page

      :returns: string

   .. php:method:: Output::getMessages()

      Returns all the messages to be displayed

      :returns: string

   .. php:method:: Output::setPageDescription()

      Returns a description for the current page

      :returns: string

   .. php:method:: Output::setPageAuthor()

      Returns an Author for the current page

      :returns: string

   .. php:method:: Output::setTemplate()

      Sets the output template, used in views
      NOTE: These are different from layouts

      :returns: Output

   .. php:method:: Output::setLanguage()

   .. php:method:: Output::getInstance()

      Returns an instance of the Library\\Output class

      :staticvar: self $instance

      :returns: self

   .. php:method:: Output::position()

      Defines positions in the template

      :param type $name:
      :param type $default:
      :param type $style:

   .. php:method:: Output::hasPosition()

      Determines if there is stuff to be loaded into that position

      :param type $name:

      :returns: boolean $rue if or false if not

   .. php:method:: Output::addToPosition()

      Adds positional data for the output

      :param type $name:
      :param type $default:
      :param type $callback: