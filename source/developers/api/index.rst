=================
Api Documentation
=================

The developers api reference

---------
Libraries
---------

.. toctree::
    :maxdepth: 2

    action/class
    cache/class
    config/class
    database/class
    date/class
    encrypt/class
    event/class
    exception/class
    folder/class
    i18n/class
    log/class
    observer/class
    output/class
    router/class
    session/class
    uri/class
    validate/class


---------
Utilities
---------

.. toctree::
    :maxdepth: 2
    
    application/class
    authenticate/class
    authorize/class
    controller/class
    debugger/class
    entity/class
    error/class
    graph/class
    inflector/class
    loader/class
    mailer/class
    model/class
    protocol/class
    registry/class
    tick/class
    user/class
    version/class
    view/class
