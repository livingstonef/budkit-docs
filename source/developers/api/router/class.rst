===============
Library\\Router
===============

.. php:namespace:: Library

.. php:class:: Router

   .. php:attr:: $view

      :var: string

   .. php:attr:: $format

      :var: string

   .. php:attr:: $routes

      Holds all the predefined Routes

      :var: array

   .. php:attr:: $resolved

      Changes to true when successfully mapped

      :var: boolean

   .. php:attr:: $parameters

      :var: type

   .. php:attr:: $variables

      :var: type

   .. php:attr:: $uri

   .. php:method:: Router::__construct()

      Constructs the Router class

      :param string $path:

   .. php:method:: Router::getApplication()

      Returns the Application

      :returns: string

   .. php:method:: Router::getMap()

      Returns the router maps

      :returns: array

   .. php:method:: Router::getController()

      Returns the called controller

   .. php:method:: Router::getMethod()

      Returns the called method

   .. php:method:: Router::getAction()

      To prevent confusion and to simplify
      This method is an alias of the getController method

      :returns: type

   .. php:method:: Router::getCommand()

      Alias of the getMethod

      :returns: type

   .. php:method:: Router::getView()

      Determines the view from the request, Passes to the dispatcher

      The default view is index, automatically loaded
      to the controller and can be access by $this->view ;

      e.g domain.com/user/profile/read/19432/feed.json => the view is 'feed' the format is 'json'
      e.g domain.com/index.php => The view is 'index' and the format 'php' will resolve to php

      :returns: string $ame of the view

   .. php:method:: Router::setView()

      Sets the view defined in the request

      :param type $name:

      :returns: Router

   .. php:method:: Router::getFormat()

      Determines the Format of the current
      Request

      :returns: string $ormat

   .. php:method:: Router::setFormat()

      Sets the Format defined in the request

      :param type $type:

      :returns: Router

   .. php:method:: Router::getParameter()

      Returns a stored parameter

      :param string $name:
      :param mixed $default:

      :returns: mixed

   .. php:method:: Router::setParameters()

      Sets the parameter array

      :param type $paramArray:

   .. php:method:: Router::setParameter()

      Set an value to a request parameter

      :param type $name:
      :param type $value:

   .. php:method:: Router::findRoute()

      Determines what route to follow

      :param type $query:
      :param type $queryVars:

      :returns: type

   .. php:method:: Router::setUrl()

      Sets the requests' universal resource link

      :param type $url:

      :returns: Router

   .. php:method:: Router::setRequestVar()

      Sets a query variable

      :param type $varname:
      :param type $value:

   .. php:method:: Router::getRequestVars()

      Please note that variables obtained here will be unsanitized

      :todo: Will $eed to use $input->getVar to get healthy variables;

      :returns: type

   .. php:method:: Router::setDynamicElement()

      Adds a dynamical element to the map

      :param string $element:
      :param string $value:

      :returns: Router

   .. php:method:: Router::unloadRoutes()

      Removes the loaded route maps after execution
      Just an attempt to keep memory usage down

      :returns: void

   .. php:method:: Router::loadRoutes()

      Loads all the router map required for routing

      :returns: array $this->routes

   .. php:method:: Router::getRoute()

      Returns an id specific route map

      :param type $routeid:

      :returns: type

   .. php:method:: Router::getRoutes()

      Returns all loaded routes

      :returns: type

   .. php:method:: Router::getURL()

      Reassembles a URL from params

      :param string $routeid:
      :param array $params:

   .. php:method:: Router::setController()

      stores the controller to be mapped to

      :param string $controller:

      :returns: Router

   .. php:method:: Router::setPath()

      The default router mapping path

      :param string $path:

      :returns: Router

   .. php:method:: Router::getPath()

      Returns the value of the Path property

      :returns: type

   .. php:method:: Router::getRealPath()

      Resolves the actuall action URL from request URL

      :returns: and $ninternalized path

   .. php:method:: Router::setApplication()

      Sets the routed application for dispatching

      :param type $application:

      :returns: Router

   .. php:method:: Router::setMethod()

      Sets the routed method for dispatching

      :param type $method:

      :returns: Router

   .. php:method:: Router::redirect()

      Executes post dispatch redirect

      :param type $url:
      :param type $code:
      :param type $message:

   .. php:method:: Router::getInstance()

      Returns an instance of the router class

      :staticvar: Router $instance

      :returns: Router