=================
Library\\Observer
=================

.. php:namespace:: Library

.. php:class:: Observer

   .. php:method:: Observer::execute()

   .. php:method:: Observer::getInstance()

      Gets an instance of the registry element

      :staticvar: self $instance
      :param type $name:

      :returns: self