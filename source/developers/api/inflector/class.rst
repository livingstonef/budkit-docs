===================
Platform\\Inflector
===================

.. php:namespace:: Platform

.. php:class:: Inflector

   .. php:method:: Inflector::pluralize()

      Pluralizes English nouns.

      :author: $Bermi Ferrer Martinez

      :copyright: Copyright $c) 2002-2006, Akelos Media, S.L. http://www.akelos.org

      :license: GNU $esser General Public License

      :since: 0.1

      :version: $Revision $.1 $

      :access: public

      :static:
      :param string $word: English noun to pluralize

      :returns: string $lural noun

   .. php:method:: Inflector::singularize()

      Singularizes English nouns.

      :author: $Bermi Ferrer Martinez

      :copyright: Copyright $c) 2002-2006, Akelos Media, S.L. http://www.akelos.org

      :license: GNU $esser General Public License

      :since: 0.1

      :access: public

      :static:
      :param string $word: English noun to singularize

      :returns: string $ingular noun.

   .. php:method:: Inflector::linearize()

   .. php:method:: Inflector::camelize()

   .. php:method:: Inflector::underscore()

   .. php:method:: Inflector::punctuate()

   .. php:method:: Inflector::ordinalize()

   .. php:method:: Inflector::getInstance()

      :staticvar: Loader $instance
      :param type $namespace:
      :param type $dir:

      :returns: Loader