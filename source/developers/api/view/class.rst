==============
Platform\\View
==============

.. php:namespace:: Platform

.. php:class:: View

   .. php:attr:: $output

   .. php:method:: View::__construct()

      Just the constructor

      :returns: void

   .. php:method:: View::email()

      Generates html output for the email

      :param type $subject:
      :param type $body:
      :param type $vars:

   .. php:method:: View::editor()

   .. php:method:: View::set()

      Sets an output

      :param string $name:
      :param mixed $value:

      :final:

      :access: public

      :returns: void $his method does not return any value

   .. php:method:: View::get()

      Gets a stored output variable

      :param string $name:
      :param string $default:
      :param string $format:

      :final:

      :access: public

      :returns: mixed

   .. php:method:: View::display()

      The default method for each controller

      :returns: void

      :abstract:

      :access: public