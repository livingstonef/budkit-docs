================
Platform\\Entity
================

.. php:namespace:: Platform

.. php:class:: Entity

   .. php:attr:: $propertyData

   .. php:attr:: $propertyModel

   .. php:attr:: $objectId

   .. php:attr:: $objectType

   .. php:attr:: $objectURI

   .. php:attr:: $valueGroup

   .. php:attr:: $listOrderByStatement

   .. php:attr:: $listLookUpConditions

   .. php:attr:: $listLookUpConditionProperties

   .. php:attr:: $withConditions

   .. php:attr:: $savedObjectURI

   .. php:attr:: $registry

   .. php:method:: Entity::__construct()

   .. php:method:: Entity::setPropertyValue()

      Sets the property Value before save

      :param string $property: Proprety ID or Property Name
      :param type $value:

   .. php:method:: Entity::setObjectType()

      Set the Entity Type for the current Entity

      :param string $entityType:

      :returns: void

   .. php:method:: Entity::getObjectType()

      Get the Entity Type for the current Entity

      :param string $entityType:

      :returns: void

   .. php:method:: Entity::setLastSavedObjectURI()

      Set the Entity Type for the current Entity

      :param string $entityType:

      :returns: void

   .. php:method:: Entity::getLastSavedObjectURI()

      Get the Entity Type for the current Entity

      :param string $entityType:

      :returns: void

   .. php:method:: Entity::setObjectId()

      Set the Entity Type for the current Entity

      :param string $entityType:

      :returns: void

   .. php:method:: Entity::getObjectId()

      Get the Entity Type for the current Entity

      :param string $entityType:

      :returns: void

   .. php:method:: Entity::setObjectURI()

      :param type $objectURI:

      :returns: \\Platform\\Entity

   .. php:method:: Entity::getObjectURI()

      :param type $objectURI:

      :returns: \\Platform\\Entity

   .. php:method:: Entity::getPropertyDefinition()

      Returns the property definition for a given property by name
      Use {static::getProperNameFromId} to get the propery name from an Id

      :param string $propertyName:

      :returns: array

   .. php:method:: Entity::getPropertyValue()

      Returns an entity property value by propery name if exists

      :todo: Allow $or default value setting;
      :param string $propertyName:
      :param interger $objectIdId:

      :returns: mixed

   .. php:method:: Entity::getObjectsByPropertyValueBetween()

      Return Object lists with matched properties between two values

      :param type $property:
      :param type $valueA:
      :param type $valueB:
      :param type $select:
      :param type $objectType:
      :param type $objectURI:
      :param type $objectId:

   .. php:method:: Entity::getObjectsByPropertyValueLike()

      Return Object lists with properties values similar to defined value (in part or whole)

      :param type $property: the property name or alias used in searching. MUST be included in the select array. @TODO group concat property array for searching in multiple fields
      :param type $value: the value of the propery name being searched for;
      :param type $select:
      :param type $objectType:
      :param type $objectURI:
      :param type $objectId:

   .. php:method:: Entity::setPropertyValueCondition()

      Sets a property conditional value for the select query

      :param type $property:
      :param type $value:

   .. php:method:: Entity::getObjectsByPropertyValueMatch()

      Return Object lists with properties matching the given value

      :param type $properties: list of properties to match to values, must have exactly a value pair in the values array and must be included in the select array
      :param type $values:
      :param type $select:
      :param type $objectType:
      :param type $objectURI:
      :param type $objectId:

   .. php:method:: Entity::setListOrderBy()

      Sets the list order direction

      :param type $fields: comma seperated list, or array
      :param type $direction:

      :returns: \\Platform\\Entity

   .. php:method:: Entity::getListOrderByStatement()

      Returns the list orderby statement if any defined or NULL if none

      :returns: string

   .. php:method:: Entity::setListLookUpConditions()

      Sets lookup conditions for entity table search

      :param type $key:
      :param type $value:
      :param type $type:
      :param type $exact:
      :param type $escape:

      :returns: \\Platform\\Entity

   .. php:method:: Entity::getListLookUpConditionsClause()

      Returns the list select clause additional conditions

      :returns: string $r null if no conditions

   .. php:method:: Entity::resetListLookUpConditions()

      Resets the list lookUpconditions after each query;

   .. php:method:: Entity::getObjectsList()

      Returns objects lists table with attributes list and values

      :param type $objectType:
      :param type $attributes:

      :returns: type $statement

   .. php:method:: Entity::getObjectsListCount()

      Gets the object List Count

      :param type $objectType:
      :param type $properties:
      :param type $objectURI:
      :param type $objectId:

      :returns: type

   .. php:method:: Entity::getObjectById()

   .. php:method:: Entity::loadObjectByURI()

      Return an Unique Object with or without attributes attributes

      :param type $objectURI:
      :param type $attributes:

   .. php:method:: Entity::loadObjectById()

      Return an Unique Object with or without attributes attributes

      :param type $objectId:
      :param type $attributes:

   .. php:method:: Entity::bindPropertyData()

   .. php:method:: Entity::removeObject()

      Saves an object to the EAV database

      :param type $objectURI:
      :param type $objectType:

      :returns: boolean

   .. php:method:: Entity::removeObjectProperty()

   .. php:method:: Entity::saveObject()

      Saves an object to the EAV database

      :param type $objectURI:
      :param type $objectType:

      :returns: boolean

   .. php:method:: Entity::getPropertyModel()

      Returns the current data model

      :returns: type

   .. php:method:: Entity::getPropertyData()

      Returns the current data model values

      :returns: type

   .. php:method:: Entity::extendPropertyModel()

      Extends the parent data model
      Allows the current object to use parent object properties

      :param type $dataModel: array(property_name=>array("label"=>"","datatype"=>"","charsize"=>"" , "default"=>"", "index"=>FALSE, "allowempty"=>FALSE))

   .. php:method:: Entity::definePropertyModel()

      Creates a completely new data model.
      Any Properties not explicitly described for this object will be ignored

      :param type $dataModel:

   .. php:method:: Entity::defineValueGroup()

      Defines a sub table for value data;

      :param type $valueGroup:

   .. php:method:: Entity::getValueGroup()

      Returns the value group

      :returns: string

   .. php:method:: Entity::display()

      Pivot this entity into a sparse matrix

      :returns: array $ssociative array

   .. php:method:: Entity::getInstance()

      Return an instance of the Object

      :staticvar: self $instance

      :returns: \\self