==============
Platform\\User
==============

.. php:namespace:: Platform

.. php:class:: User

   .. php:attr:: $authenticated

   .. php:attr:: $authority

   .. php:attr:: $instance

   .. php:method:: User::getInstance()

      Returns an instance of the User Object

      :returns: type

   .. php:method:: User::_()

      Returns the user object with the current use loaded

      :param type $id:

   .. php:method:: User::isAuthenticated()

      Method to determine if the user is authenticated;

      :returns: type

   .. php:method:: User::getAuthenticated()

      Determins if a user is authenticated

      :returns: type

   .. php:method:: User::__construct()

      Constructs the user proxy object

      :param type $userid: