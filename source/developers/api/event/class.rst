==============
Library\\Event
==============

.. php:namespace:: Library

.. php:class:: Event

   .. php:method:: Event::loadHooks()

      Loads system event hooks

      :param string $ext:
      :param string $path:

   .. php:method:: Event::getInstance()

      Gets an instance of the Library\\Exception Object

      :static: self $instance

      :returns: self