===============
Platform\\Graph
===============

.. php:namespace:: Platform

.. php:class:: Graph

   .. php:attr:: $instance

   .. php:attr:: $nodeSet

      A vertex (pl. vertices) or node is the fundamental unit of which a graph is formed.
      An array object containing graph vertex set.

      :var: type

   .. php:attr:: $edgeSet

      Undirected edge between two endpoints in vertice set of undirected graph

      :var: type

   .. php:attr:: $arcSet

      Directed edge between two endpoints in vertex set, Holds
      Edge IDs that have been added as arcs..

      :var: type

   .. php:attr:: $subgraphs

      An array of sub graphs objects

      :var: type

   .. php:method:: Graph::getPath()

      Determines the shortest distance between two nodes
      d(u,v)

      :param type $nodeA:
      :param type $nodeB:

      :returns: array(); $n ordered array sequence of nodes from u to v

   .. php:method:: Graph::getPathLength()

      The Path length |d(u,v)| is the total number of edges in the path connecting
      nodeA to nodeB.

      :param type $nodeA:
      :param type $nodeB:

      :reurns: interger $ pathlength of zero implies infinity, i.e no path was found

   .. php:method:: Graph::getSize()

      The size of a graph is the number of its edges |E(G)|

      :returns: interger

   .. php:method:: Graph::getOrder()

      The order of a graph is the number of its nodes/vertices |V(G)|

      :returns: interger

   .. php:method:: Graph::getArcSet()

      Returns an array of edgeIds for directed edges (arcs)

      :returns: type

   .. php:method:: Graph::getEdgeSet()

      Returns all edges describing this graph

      :returns: type

   .. php:method:: Graph::getMaxDegree()

      Returns the maximum degree incident on graph nodes

      :returns: interger

   .. php:method:: Graph::getMinDegree()

      Returns the minimum degree.
      Degrees are a represenation of the number of degrees
      incident to a node.

   .. php:method:: Graph::getIsolated()

      Isolated nodes are nodes with a degree of zero

      :returns: array

   .. php:method:: Graph::getLeaves()

      Returns all nodes with a degree of 1;

      :returns: array;

   .. php:method:: Graph::getNode()

      Returns a node object if exists in graph

      :param type $nodeId: case sensitive ID of the node requested

      :returns: object $node if found;

   .. php:method:: Graph::addEdge()

      Adds an edge between two node endpoints.

      :param type $nodeA:
      :param type $nodeB:
      :param type $name:
      :param type $data:
      :param type $directed:
      :param type $weight:

      :returns: boolean

   .. php:method:: Graph::addNode()

      Adds a node to the current graph

      :param type $node:

   .. php:method:: Graph::removeNode()

      Removes a node from the graph

      :param type $nodeId:

   .. php:method:: Graph::removeEdge()

      Removes an Edge from the graph. Use removeArc to remove directed edges

      :param type $head:
      :param type $tail:
      :param type $directed: if false, will remove all incident edges of the kind head-tail or tail-head

      :returns: boolean

      :throws: \\Platform\\Exception

   .. php:method:: Graph::isDirected()

      Checks if the current graph is a directed graph

      :returns: boolean $rue if directed and false if not;

   .. php:method:: Graph::__construct()

      Constructs a new graph.

      :param type $nodes:
      :param type $edges:
      :param type $directed:
      :param type $graphID:

   .. php:method:: Graph::createNode()

      Creates and adds a Node to the graph if none, already exists

      :param type $nodeId:
      :param type $data:

   .. php:method:: Graph::getInstance()

      Returns and instantiated Instance of the graph class

      NOTE: As of PHP5.3 it is vital that you include constructors in your class
      especially if they are defined under a namespace. A method with the same
      name as the class is no longer considered to be its constructor

      :staticvar: object $instance

      :property-read: object $instance To determine if class was previously instantiated

      :property-write: object $instance

      :returns: object $raph