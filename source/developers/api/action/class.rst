===============
Library\\Action
===============

.. php:namespace:: Library

.. php:class:: Action

   .. php:attr:: $state

      The State of an action controller propergated to observers

      :var: type

   .. php:attr:: $stateParams

   .. php:attr:: $observers

      The observers listening to the actions performed by this controller

      :var: type

   .. php:method:: Action::attach()

   .. php:method:: Action::detach()

   .. php:method:: Action::actionStateAs()

   .. php:method:: Action::getState()

   .. php:method:: Action::getObservers()

   .. php:method:: Action::notify()

   .. php:method:: Action::getInstance()

      Gets an instance of the registry element

      :staticvar: self $instance
      :param type $name:

      :returns: self