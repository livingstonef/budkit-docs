============
Library\\Uri
============

.. php:namespace:: Library

.. php:class:: Uri

      A library providing URI and URL parsing capability

      The main purpose of this class is to automatically determine the key components
      pertaining to identifying the requested resource as well as build resource identifiers
      to system resources and actions. Whilst, tt does not provide any routing capability,
      this class is crucial to routing user queries to appropriate actions. cf \\Library\\Router

   .. php:attr:: $path

      Path

      :var: string

   .. php:method:: Uri::__construct()

      Constructor for the URI Library Object

      :returns: void

   .. php:method:: Uri::compile()

      Uses a route map ti compile a query?

      :param array $partsArray:

      :returns: string $ well formed internalized URL from parts

   .. php:method:: Uri::_()

      An alias for the internal pointer

      :param string $url:

      :returns: string

   .. php:method:: Uri::internal()

      Resolves a url adds path if missing

      :param string $url: THe Url to internalize

      :returns: string $ well formed internalized URL

   .. php:method:: Uri::externalize()

      Adds the schema, host and path to an internal url 'path'

      :param type $path:

   .. php:method:: Uri::internalize()

      Alias for internalize

      :param type $url:

      :returns: type

   .. php:method:: Uri::getCredentials()

      Determines any authentication credentials embeded in URIs

      :param string $paramname:
      :param mixed $default:

      :returns: boolean $alse if not found, credential if found

   .. php:method:: Uri::getQuery()

      Returns the query string

      :returns: string

   .. php:method:: Uri::getQueryVariables()

      Returns all variables found in the request query

      :returns: type

   .. php:method:: Uri::getHost()

      Returns the request host

      :returns: string

   .. php:method:: Uri::getPath()

      Get path from uri

      :returns: string

   .. php:method:: Uri::getURL()

      Reassembles a URL from params

      :param string $routeid:
      :param array $params:

   .. php:method:: Uri::getScheme()

      Returns the request protocol

      :returns: string

   .. php:method:: Uri::setPath()

      Sets the URL path

      :param string $path:

   .. php:method:: Uri::getScriptName()

      Returns the script name

      :returns: string

   .. php:method:: Uri::getInstance()

      Gets an instance of the URI Library object

      :staticvar: Uri $instance

      :returns: Uri