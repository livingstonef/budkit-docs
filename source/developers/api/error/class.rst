===============
Platform\\Error
===============

.. php:namespace:: Platform

.. php:class:: Error

   .. php:const:: Error:: APPLICATION_ERROR = 9033;

   .. php:const:: Error:: PLATFORM_ERROR = 9032;

   .. php:const:: Error:: LIBRARY_ERROR = 9031;

   .. php:method:: Error::raise()

      Raises an error

      :param type $errorCode:
      :param type $errorMsg:

   .. php:method:: Error::shutdown()

   .. php:method:: Error::handler()

      Displays an Error to the front user

      :param type $errNo:
      :param type $errMsg:
      :param type $file:
      :param type $line:

      :returns: type

   .. php:method:: Error::exception()

      Handles a cauth Exception

      :param type $exception: