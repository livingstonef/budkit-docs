==================
Platform\\Protocol
==================

.. php:namespace:: Platform

.. php:class:: Protocol

   .. php:attr:: $options

   .. php:attr:: $protocol

   .. php:method:: Protocol::__construct()

   .. php:method:: Protocol::configure()

   .. php:method:: Protocol::configureProtocol()

      Configures the protocol option

      :throws: InvalidArgumentException