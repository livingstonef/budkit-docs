===================
Platform\\Authorize
===================

.. php:namespace:: Platform

.. php:class:: Authorize

   .. php:attr:: $userid

      The user ID, whose authority and permission is being evaluated

      :var: string $userid

   .. php:attr:: $impliedAuthority

      The currently authenticated users authority

      :var: string $ontstant

   .. php:attr:: $permissions

      The directly awarded authority permission of current user

      :var: array

   .. php:attr:: $areas

      The implied authorities/permissions

      :var: areas

   .. php:method:: Authorize::setAuthority()

      Sets the current user's authority

      :param type $implied:

      :property-write: string $impliedAuthority

      :returns: void

   .. php:method:: Authorize::getAuthority()

      Determines the current user (in session) 's authority or that of a specified user id

      :param string $userid:

      :property-read: string $impliedAuthority

      :returns: string $he implied Authority of the user

   .. php:method:: Authorize::getAuthorityTree()

      Returns the authority tree

      :uses: Library\\Datbase $o get the user authority tree

      :returns: Array;

   .. php:method:: Authorize::getPermissions()

      Gets the permissions givent to the authenticated users

      :param object $authenticated:

      :uses: \\Library\\Authorize\\Permission $o determin execute permissions

      :returns: object $ermission

   .. php:method:: Authorize::setPermission()

      Sets the object permission

      :param type $object:
      :param type $permission:

   .. php:method:: Authorize::can()

      Checks that the modifier (user/or even machine) defined (definition)
      has the right authority (group) and that the authority (group) has the
      right permission (read/write/execute) defined (in definition) to the
      interact with the action

      DEFINITIONS

      Authority can "perform task"

      :param string $permission:
      :param string $object: URI reference

      :returns: void

   .. php:method:: Authorize::__construct()

      Authorize constructor. Checks if authenticated and store authenticated

      :param type $definition:

   .. php:method:: Authorize::getInstance()

      Get's an instance of the authority class

      :staticvar: self $instance

      :property-read: object $instance To determine if class was previously instantiated

      :property-write: object $instance
      :param type $definition:

      :returns: self