============
Library\\Log
============

.. php:namespace:: Library

.. php:class:: Log

   .. php:attr:: $log

   .. php:attr:: $file

      A file resource where log message are stored

      :var: string

   .. php:attr:: $enable

      Determines whether to enable the log or not

      :var: boolean

   .. php:method:: Log::message()

      Stores a message in the log file

      :param type $string:
      :param type $type:

   .. php:method:: Log::error()

      Stores an error message in the log file

      :param type $string:
      :param type $code:

   .. php:method:: Log::getFile()

      Returns the contents of the log file

      :returns: string

   .. php:method:: Log::getLastMessage()

      Returns the last message stored in the log

      :returns: array

   .. php:method:: Log::dump()

      Dumps the console log

      :returns:

   .. php:method:: Log::setLog()

   .. php:method:: Log::getLog()

   .. php:method:: Log::setMode()

   .. php:method:: Log::getConsole()

   .. php:method:: Log::__construct()

   .. php:method:: Log::_()

      Logs a system message

      :TODO: relegate $his function to the log class
      :param type $msgString:
      :param type $console:
      :param type $type:
      :param type $typekey:
      :param type $title:
      :param type $logFile:

      :returns: type

   .. php:method:: Log::getInstance()

      Gets an instance of the logger class

      :staticvar: self $instance

      :returns: self