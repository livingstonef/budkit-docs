================
Platform\\Loader
================

.. php:namespace:: Platform

.. php:class:: Loader

   .. php:attr:: $objects

   .. php:const:: Loader::  GLOBAL_NAMESPACE = "";

   .. php:attr:: $application

      :var: type

   .. php:attr:: $path

      :var: type

   .. php:attr:: $namespace

      :var: type

   .. php:method:: Loader::__construct()

      :param type $namespace:
      :param type $path:

   .. php:method:: Loader::model()

      :param type $model:

      :returns: type

   .. php:method:: Loader::table()

      Get table

      :param type $table:
      :param type $primayId:

      :returns: type

   .. php:method:: Loader::controller()

      Loads a controller.

      :param type $controller:
      :param type $app:
      :param type $vars:
      :param type $return:
      :param type $namespace:

      :returns: type

      :todo: Needs $ome reworking

   .. php:method:: Loader::view()

      Loads the view from within an application!

      :param string $view:
      :param array $vars:
      :param boolean $return:
      :param string $namespace:

      :returns: object $age

   .. php:method:: Loader::layout()

      Gets a layout

      :param type $layout:
      :param type $app:
      :param type $return: if false returns the url

   .. php:method:: Loader::library()

      :param type $library:

      :returns: type

   .. php:method:: Loader::helper()

      :param type $helper:

      :returns: type

   .. php:method:: Loader::setApplication()

   .. php:method:: Loader::addSearchPath()

   .. php:method:: Loader::getInstance()

      :static: var $oader $instance
      :param type $namespace:
      :param type $dir:

      :returns: Loader

   .. php:method:: Loader::__invoke()

      :param type $class: