=============
Library\\i18n
=============

.. php:namespace:: Library

.. php:class:: i18n

   .. php:attr:: $instance

   .. php:attr:: $category

      The current translation string category

      :var: type

   .. php:attr:: $language

      The language for current locale

      :var: string

   .. php:attr:: $langKey

      ISO 639-3 for current locale

      :var: string

   .. php:attr:: $locale

      Locale

      :var: string

   .. php:attr:: $default

      Default ISO 639-3 language.

      DEFAULT_LANGUAGE is defined in an application this will be set as a fall back

      :var: string

   .. php:attr:: $charset

      Encoding used for current locale

      :var: string

   .. php:attr:: $direction

      Text direction for current locale

      :var: string

   .. php:attr:: $found

      Set to true if a locale is found

      :var: string

   .. php:attr:: $domain

      The current domain being translated

      :var: string

   .. php:attr:: $translations

      Extracted Translations grouped into domains

      :var: array

   .. php:attr:: $_l10nMap

      Maps ISO 639-3 to I10n::_l10nCatalog

      :var: array

   .. php:attr:: $_l10nCatalog

      HTTP_ACCEPT_LANGUAGE catalog

      holds all information related to a language

      :var: array

   .. php:method:: i18n::setDomain()

      A domain could be an

      :param string $domain:
      :param string $path:

   .. php:method:: i18n::getTranslations()

      Returns the translations cache;

      :param string $domain:
      :param string $category:

      :returns: array

   .. php:method:: i18n::setLanguage()

      Set the string representation for the current locale

      :param type $lang:

   .. php:method:: i18n::setLocale()

      Set the current locale for translations

      :param type $locale:

   .. php:method:: i18n::getLocale()

      Returns a string representation for the current locale

      :returns: string

   .. php:method:: i18n::getLanguage()

      Returns a string title for the current language

      :returns: string

   .. php:method:: i18n::getLanguageDefinition()

      Returns an array containing the full language/locale defintion

      :param string $locale:

      :returns: array

   .. php:method:: i18n::translate()

      Translates a string into a valid

      :param type $singular:
      :param type $plural:
      :param type $domain:
      :param type $category:
      :param type $count:
      :param type $language:

   .. php:method:: i18n::getInstance()

      Returns and instantiated Instance of the i18n class

      NOTE: As of PHP5.3 it is vital that you include constructors in your class
      especially if they are defined under a namespace. A method with the same
      name as the class is no longer considered to be its constructor

      :staticvar: object $instance

      :property-read: object $instance To determine if class was previously instantiated

      :property-write: object $instance

      :returns: object $18n