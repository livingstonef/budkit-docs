===================
Library\\Validation
===================

.. php:namespace:: Library

.. php:class:: Validate

      Validation class for semi-auto validating user input

      IMPORTANT: This validation class is not a sanitization class. The Input library
      Should be used for all filteration and sanitization needs.

   .. php:method:: Validate::__construct()

      Construct the Validate class

      :returns: void

   .. php:method:: Validate::string()

      Uses a custom RegEx Pattern to validate a string. Or alternatively is_string()

      :param string $str: The input string to be validated
      :param string $regExp: A custom input validation pattern
      :param interger $length: Optional string length specification

      :returns: booleant $rue or false. Returns true if validation rules met, or false otherwise

   .. php:method:: Validate::boolean()

      Validates a boolean datatype. Wrapper method for is_bool()

      :param mixed $bool: the input data

      :returns: boolean $eturns true if datatype is boolean

   .. php:method:: Validate::decimal()

      Validates a decimal

      :param string $dec:

      :returns: boolean $rue if is true decimal, False if not

   .. php:method:: Validate::alphaNumeric()

      Validates a character is alphanumeric

      :param string $alnum:
      :param interger $length:

      :returns: boolean $rue if is alphanumeric, false if not

   .. php:method:: Validate::timestamp()

      Checks that a string is a timestamp

      :param string $tstamp:

      :returns: boolean $rue if is timestamp, false if not;

   .. php:method:: Validate::float()

      Validate if a string is a float.

      :param string $flt:

      :returns: boolean $rue if is float, False if not.

   .. php:method:: Validate::number()

      Checks input variable $num is a number

      :param mixed $num: input data

      :returns: boolean $rue if is number, False if not

   .. php:method:: Validate::interger()

      Validates an interger. Checks input $int is an Interger datatype

      :param mixed $int:

      :returns: boolean $rue if is interger, False if its not.

   .. php:method:: Validate::IP()

      Validates an ip address format

      :param string $address:

      :returns: boolean $rue if it is a valid IP address, False if Not.

   .. php:method:: Validate::email()

      Quick and easy email validation

      :param string $email:

      :returns: boolean $rue if is valid email address, False if not

   .. php:method:: Validate::getInstance()

      Gets an instance of the validate object

      :staticvar: self $instance

      :returns: self