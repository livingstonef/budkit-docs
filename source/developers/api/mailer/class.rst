================
Platform\\Mailer
================

.. php:namespace:: Platform

.. php:class:: Mailer

   .. php:attr:: $useragent

      Used as the User-Agent and X-Mailer headers' value.

      :var: string

   .. php:attr:: $mailpath

      Path to the Sendmail binary.

      :var: string

   .. php:attr:: $protocol

      Which method to use for sending e-mails.

      :var: string $mail', 'sendmail' or 'smtp'

   .. php:attr:: $smtpHost

      STMP Server host

      :var: string

   .. php:attr:: $smtpUser

      SMTP Username

      :var: string

   .. php:attr:: $smtpPass

      SMTP Password

      :var: string

   .. php:attr:: $smtpPort

      SMTP Server port

      :var: int

   .. php:attr:: $smtpTimeout

      SMTP connection timeout in seconds

      :var: int

   .. php:attr:: $smtpKeepalive

      SMTP persistent connection

      :var: bool

   .. php:attr:: $smtpCrypto

      SMTP Encryption

      :var: string $mpty, 'tls' or 'ssl'

   .. php:attr:: $wordwrap

      Whether to apply word-wrapping to the message body.

      :var: bool

   .. php:attr:: $wrapchars

      Number of characters to wrap at.

      :see: CI_Email::$wordwrap

      :var: int

   .. php:attr:: $mailtype

      Message format.

      :var: string $text' or 'html'

   .. php:attr:: $charset

      Character set (default: utf-8)

      :var: string

   .. php:attr:: $multipart

      Multipart message

      :var: string $mixed' (in the body) or 'related' (separate)

   .. php:attr:: $altMessage

      Alternative message (for HTML messages only)

      :var: string

   .. php:attr:: $validate

      Whether to validate e-mail addresses.

      :var: bool

   .. php:attr:: $priority

      X-Priority header value.

      :var: int $-5

   .. php:attr:: $newline

      Newline character sequence.
      Use "\\r\\n" to comply with RFC 822.

      :link: http://www.ietf.org/rfc/rfc822.txt

      :var: string $\r\n" or "\\n"

   .. php:attr:: $crlf

      CRLF character sequence

      RFC 2045 specifies that for 'quoted-printable' encoding,
      "\\r\\n" must be used. However, it appears that some servers
      (even on the receiving end) don't handle it properly and
      switching to "\\n", while improper, is the only solution
      that seems to work for all environments.

      :link: http://www.ietf.org/rfc/rfc822.txt

      :var: string

   .. php:attr:: $dsn

      Whether to use Delivery Status Notification.

      :var: bool

   .. php:attr:: $sendMultipart

      Whether to send multipart alternatives.
      Yahoo! doesn't seem to like these.

      :var: bool

   .. php:attr:: $bccBatchMode

      Whether to send messages to BCC recipients in batches.

      :var: bool

   .. php:attr:: $bccBatchSize

      BCC Batch max number size.

      :see: CI_Email::$bcc_batch_mode

      :var: int

   .. php:attr:: $subject

      Subject header

      :var: string

   .. php:attr:: $body

      Message body

      :var: string

   .. php:attr:: $finalbody

      Final message body to be sent.

      :var: string

   .. php:attr:: $altBoundary

      multipart/alternative boundary

      :var: string

   .. php:attr:: $atcBoundary

      Attachment boundary

      :var: string

   .. php:attr:: $headerStr

      Final headers to send

      :var: string

   .. php:attr:: $smtpConnect

      SMTP Connection socket placeholder

      :var: resource

   .. php:attr:: $encoding

      Mail encoding

      :var: string $8bit' or '7bit'

   .. php:attr:: $smtpAuth

      Whether to perform SMTP authentication

      :var: bool

   .. php:attr:: $replytoFlag

      Whether to send a Reply-To header

      :var: bool

   .. php:attr:: $recipients

      Recipients

      :var: string[]

   .. php:attr:: $ccArray

      CC Recipients

      :var: string[]

   .. php:attr:: $bccArray

      BCC Recipients

      :var: string[]

   .. php:attr:: $headers

      Message headers

      :var: string[]

   .. php:attr:: $attachments

      Attachment data

      :var: array

   .. php:attr:: $protocols

      Valid $protocol values

      :see: CI_Email::$protocol

      :var: string[]

   .. php:attr:: $baseCharsets

      Base charsets

      Character sets valid for 7-bit encoding,
      excluding language suffix.

      :var: string[]

   .. php:attr:: $bitDepths

      Bit depths

      Valid mail encodings

      :see: CI_Email::$_encoding

      :var: string[]

   .. php:attr:: $priorities

      $priority translations

      Actual values to send with the X-Priority header

      :var: string[]

   .. php:attr:: $config

   .. php:method:: Mailer::__construct()

      Constructor - Sets Email Preferences

      The constructor can be passed an array of config values

      :param array $config: = array()

      :returns: void

   .. php:method:: Mailer::initialize()

   .. php:method:: Mailer::clear()

      Initialize the Email Data

      :param bool:

      :returns: CI_Email

   .. php:method:: Mailer::from()

      Set FROM

      :param string $from:
      :param string $name:
      :param string $return_path: = NULL Return-Path

      :returns: CI_Email

   .. php:method:: Mailer::replyTo()

      Set Reply-to

      :param string:
      :param string:

      :returns: CI_Email

   .. php:method:: Mailer::to()

      Set Recipients

      :param string:

      :returns: CI_Email

   .. php:method:: Mailer::cc()

      Set CC

      :param string:

      :returns: CI_Email

   .. php:method:: Mailer::bcc()

      Set BCC

      :param string:
      :param string:

      :returns: CI_Email

   .. php:method:: Mailer::subject()

      Set Email Subject

      :param string:

      :returns: CI_Email

   .. php:method:: Mailer::message()

      Set Body

      :param string:

      :returns: CI_Email

   .. php:method:: Mailer::attach()

      Assign file attachments

      :param string $filename:
      :param string $disposition: = 'attachment'
      :param string $newname: = NULL
      :param string $mime: = ''

      :returns: CI_Email

   .. php:method:: Mailer::setHeader()

      Add a Header Item

      :param string:
      :param string:

      :returns: void

   .. php:method:: Mailer::strToArray()

      Convert a String to an Array

      :param string:

      :returns: array

   .. php:method:: Mailer::setAltMessage()

      Set Multipart Value

      :param string:

      :returns: CI_Email

   .. php:method:: Mailer::setMailType()

      Set Mailtype

      :param string:

      :returns: CI_Email

   .. php:method:: Mailer::setWordwrap()

      Set Wordwrap

      :param bool:

      :returns: CI_Email

   .. php:method:: Mailer::setProtocol()

      Set Protocol

      :param string:

      :returns: CI_Email

   .. php:method:: Mailer::setPriority()

      Set Priority

      :param int:

      :returns: CI_Email

   .. php:method:: Mailer::setNewline()

      Set Newline Character

      :param string:

      :returns: CI_Email

   .. php:method:: Mailer::setCRLF()

      Set CRLF

      :param string:

      :returns: CI_Email

   .. php:method:: Mailer::setBoundaries()

      Set Message Boundary

      :returns: void

   .. php:method:: Mailer::getMessageId()

      Get the Message ID

      :returns: string

   .. php:method:: Mailer::getProtocol()

      Get Mail Protocol

      :param bool:

      :returns: mixed

   .. php:method:: Mailer::getEncoding()

      Get Mail Encoding

      :param bool:

      :returns: string

   .. php:method:: Mailer::getContentType()

      Get content type (text/html/attachment)

      :returns: string

   .. php:method:: Mailer::setDate()

      Set RFC 822 Date

      :returns: string

   .. php:method:: Mailer::getMimeMessage()

      Mime message

      :returns: string

   .. php:method:: Mailer::validateEmail()

      Validate Email Address

      :param string:

      :returns: bool

   .. php:method:: Mailer::validEmail()

      Email Validation

      :param string:

      :returns: bool

   .. php:method:: Mailer::cleanEmail()

      Clean Extended Email Address: Joe Smith <joe@smith.com>

      :param string:

      :returns: string

   .. php:method:: Mailer::getAltMessage()

      Build alternative plain text message

      Provides the raw message for use in plain-text headers of
      HTML-formatted emails.
      If the user hasn't specified his own alternative message
      it creates one by stripping the HTML

      :returns: string

   .. php:method:: Mailer::wordWrap()

      Word Wrap

      :param string:
      :param int $ine-length: limit

      :returns: string

   .. php:method:: Mailer::buildHeaders()

      Build final headers

      :returns: string

   .. php:method:: Mailer::writeHeaders()

      Write Headers as a string

      :returns: void

   .. php:method:: Mailer::buildMessage()

      Build Final Body and attachments

      :returns: bool

   .. php:method:: Mailer::prepQuotedPrintable()

      Prep Quoted Printable

      Prepares string for Quoted-Printable Content-Transfer-Encoding
      Refer to RFC 2045 http://www.ietf.org/rfc/rfc2045.txt

      :param string:

      :returns: string

   .. php:method:: Mailer::prepQEncoding()

      Prep Q Encoding

      Performs "Q Encoding" on a string for use in email headers.
      It's related but not identical to quoted-printable, so it has its
      own method.

      :param string:

      :returns: string

   .. php:method:: Mailer::send()

      Send Email

      :param bool $auto_clear: = TRUE

      :returns: bool

   .. php:method:: Mailer::batchBccSend()

      Batch Bcc Send. Sends groups of BCCs in batches

      :returns: void

   .. php:method:: Mailer::unwrapSpecials()

      Unwrap special elements

      :returns: void

   .. php:method:: Mailer::removeNlCallback()

      Strip line-breaks via callback

      :param string $matches:

      :returns: string

   .. php:method:: Mailer::spoolEmail()

      Spool mail to the mail server

      :returns: bool

   .. php:method:: Mailer::sendWithMail()

      Send using mail()

      :returns: bool

   .. php:method:: Mailer::sendWithSendmail()

      Send using Sendmail

      :returns: bool

   .. php:method:: Mailer::sendWithSmtp()

      Send using SMTP

      :returns: bool

   .. php:method:: Mailer::smtpConnect()

      SMTP Connect

      :returns: string

   .. php:method:: Mailer::sendCommand()

      Send SMTP command

      :param string:
      :param string:

      :returns: string

   .. php:method:: Mailer::smtpAuthenticate()

      SMTP Authenticate

      :returns: bool

   .. php:method:: Mailer::sendData()

      Send SMTP data

      :param string $data:

      :returns: bool

   .. php:method:: Mailer::getSmtpData()

      Get SMTP data

      :returns: string

   .. php:method:: Mailer::getHostname()

      Get Hostname

      :returns: string

   .. php:method:: Mailer::mimeTypes()

      Mime Types

      :param string:

      :returns: string

   .. php:method:: Mailer::getInstance()

      :staticvar: Loader $instance
      :param type $namespace:
      :param type $dir:

      :returns: Loader