===================
Collaboration Tools
===================

From private messaging, to live chat and even collaborative editing, Budkit provides the tools and resources to enable
your members stay productive wherever they are. In this section we provide guides to all these useful features