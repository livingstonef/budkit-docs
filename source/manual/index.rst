============
Usage Manual
============

Welcome to the Budkit everyday usage manual. We have subdivide this section based on individual components/features of
our platform. Click through for more information. As the software is actively being developed we encourage you to check
and re-check the notes within this pages, for minor changes that might affect how you use this software. Important changes
and updates between versions are highlighted in our change log.

---------------
User Management
---------------

.. toctree::
    :maxdepth: 2

    Introduction <users/index.rst>

----------------
Media Management
----------------

.. toctree::
    :maxdepth: 2

    Introduction <attachments/index.rst>

-------------
Collaboration
-------------

.. toctree::
    :maxdepth: 2

    Introduction <collaboration/index.rst>
