Introduction
============

Budkit is a free open-source location-aware PHP Platform and Object Oriented PHP Framework for building applications for social and professional networking. BudKit is about people, the places they attend and the things the do. Budkit requires a Database to run and supports multiple DBMS including MySQL, PostgreSQL and SQLitee3.
It is built using the hierarchical model view approach (MVC) which separates presentation from logic.
it is designed to be a highly extensible platform, as such it is difficult to define its true feature scope.