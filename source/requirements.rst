Requirements
============

The following are required to use budkit

Apache 2+
---------

We have not tested on anything other than Apache2. So expect the unexpected with any diversions. Apache's `mod_rewrite`,`mod_mysql`, `mod_xml`, `mod_zlib` modules are required.

PHP 5.4+
--------

Several of the key default features require a minimal version of PHP of 5.4. It is always advisable to keep your web server up to date to keep with security and other essential updates. We do not support and do not intend to provide any backward compatible version of our software. The following PHP extensions and directives are required

gd
    Image Manipultation

mcrypt
    Cryptography Handling

gettext
    Localization

tokenizer
    Tokenizer

pcre
    Perl Compatible RegEx

json
    javaScript Object Notation

iconv
    IconV Character-Set Conversion

imap
    IMAP extension

mbstring
    Multibyte Strings

ctype
    Character-Type checking

libxml
    XML Manipulation

zlib
    Zlib Compression

safe_mode
 Off

display_errors
    Off

magic_quotes_sybase
    Off

magic_quotes_gpc
    Off

magic_quotes_runtime
    Off

session.auto_start
    Off

output_buffering
    On

register_globals
    Off

file_uploads
    On - upload_max_filesize > 200M (for uploading large files e.g videos)


Database
--------

A database system is required to store and manage user submitted data. Our platform dictates a requirement for a relational database management system, implying at this time that we do not support database systems like MongoDB or Couch DB. Presently one of the following systems are required to run

MySQL 5.5+
    If you intend to use MySQL as your data store the minimum required version is MySQL 5.5+ or higher.


