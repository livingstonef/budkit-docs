*****************
Installing Budkit
*****************

Installing budkit should be relatively straight forward once all the :doc:`requirements` are met. We have included an
install wizard that should walk you through installation in 3 steps.

---------------
Download Latest
---------------

1. Download the `latest version of budkit <https://bitbucket.org/livingstonef/budkit/downloads>`_ (|version|)

.. note::
   If you downloaded our `master branch or latest release tag <https://bitbucket.org/livingstonef/budkit>`_, please ensure that all sub-modules our downloaded as they probably won't have been.
   In other words, download the `framework sub-module <https://bitbucket.org/livingstonef/budkit-framework>`_ separately into the framework subdirectory!

2. Move the downloaded archive to the server.

.. note::
    You can unpack the archive on your local machine and move to a remote server using FTP.
    Make sure the :file:`config.inc` file is in the root directory of your server.
    If installing in a sub-folder remember to edit the ``path`` param in the  :doc:`general config directives </developers/api/config/index>` as well as the ``RewriteBase`` in the :file:`.htaccess` file

3. If you are downloading to contribute to the code you can checkout from our repository instead::

    $ git clone -b https://bitbucket.org/livingstonef/budkit.git
    $ git submodule init
    $ git submodule update

4. Check your general :doc:`config </developers/api/config/index>` directives

-------------------------
File & Folder Permissions
-------------------------

The following file and folder permissions are required

+---------------------------------------+------------+-----------+
| File/Folder                           | chmod      |Permission |
+=======================================+============+===========+
|/applications/setup/sessions/          | 0755       |rwx        |
+---------------------------------------+------------+-----------+
|/config/                               | 0755       |rwx        |
+---------------------------------------+------------+-----------+
|/users/                                | 0755       |rwx        |
+---------------------------------------+------------+-----------+
|/public/downloads/                     | 0755       |rwx        |
+---------------------------------------+------------+-----------+

----------------------
Preparing the Database
----------------------

1. For specific information on setting up your database follow the links below

  * :doc:`MySQL </developers/api/database/drivers/mysql>`

2. Once ready you will need to note the following information

  * The database server
  * The database name
  * The database user's name
  * The database user's password